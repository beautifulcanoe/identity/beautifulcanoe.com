# beautifulcanoe.com

[![pipeline status](https://gitlab.com/beautifulcanoe/identity/beautifulcanoe.com/badges/develop/pipeline.svg)](https://gitlab.com/beautifulcanoe/identity/beautifulcanoe.com/-/commits/develop)

This is the source code for the Beautiful Canoe website.

## Deployments

The live website can be found at: [https://beautifulcanoe.com/](https://beautifulcanoe.com/).

## Documentation and planning

There is a [Trello board](https://trello.com/b/ch1O41HR/beautiful-canoe-website) for this website.
If you need access to it, please give @jbarney66 your [Trello](https://trello.com/) username.

## Developers

Original setup by Peter Lewis - @petelewis.
Contributions by Sarah Mount - @snim2.

The following student developers have contributed to this project:

* Anisah Shahid - @anisah54 (2019)
* Paul Aina (2018)
* Gavinder Hayer - @hayerg1 (2018)
* Kamran Ali - @Moonschool (2017-2019)
* Chloé Alsop - @alsopc (2017-2018)
* Amir Makanvand (2017)
* Mohammed Nazish Saghir (2017)
* Adeel Ahmed (2016-2017)

## Getting Started with Development

Please read [CONTRIBUTING.md](/CONTRIBUTING.md) before you start working on this repository.

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
