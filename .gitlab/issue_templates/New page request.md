## New page

Briefly describe the new page you wish to add to the site.

[[_TOC_]]

---

### Audience

* **Who is the intended audience for the new page?**
* **What value do you expect them to find in the new information?**
* **What sharing points (e.g. social media?) are they likely to use to share this information?**

### Content

What content should the new page have?

### Navigation

Where in the navigation structure of the website should your page appear?

/label ~"Feature Request"
