---
---

var from_students=0, to_student={{ site.data.stats.students }};
var from_years=0, to_years={{ 'now' | date: '%Y' | minus: site.data.stats.founded }};
var from_project=0, to_project={{ site.data.stats.projects }};


function increment() {
    setTimeout(function() {
        from_students++;
        if(from_students <= to_student) {
            document.getElementById("student").innerHTML = from_students;
            increment();
        }
    }, 70);
}

function increment2() {
    setTimeout(function() {
        from_years++;
        if(from_years <= to_years) {
            document.getElementById("year").innerHTML = from_years;
            increment2();
        }
    }, 200);
}

function increment3() {
    setTimeout(function() {
        from_project++;
        if(from_project <= to_project) {
            document.getElementById("project").innerHTML = from_project;
            increment3();
        }
    }, 100);
}

increment();
increment2();
increment3();
