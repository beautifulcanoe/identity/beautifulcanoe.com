# Contributing to beautifulcanoe.com

This website is written in static HTML, CSS and JavaScript and should follow our [brand guidelines](https://brand.beautifulcanoe.com/).

The website is automatically deployed upon push to the `main` branch.

[[_TOC_]]

---

## Getting started

### Clone this repository

From the command line, run:

```shell
git clone git@gitlab.com:beautifulcanoe/identity/beautifulcanoe.com.git
cd beautifulcanoe.com
```

### Prerequisites

#### Ruby

Ensure Ruby is installed on your computer. To check run the following command:

```shell
ruby -v
```

If you do not have it, install it from [Ruby Website](https://www.ruby-lang.org/en/downloads/)

#### Bundler

Ensure Bundler is installed on your computer. To check run the following command:

```shell
bundler -v
```

If you do not have it, install it with the following command:

```shell
gem install bundler
```

After installing `Bundler`, run the following command to install all the dependencies needed for this project. These can be found on `Gemfile`

```shell
bundle install
```

### Install Docker

If you are working on Ubuntu, this should work for you:

```shell
sudo apt-get install docker docker.io docker-compose
```

It is also a good idea to add yourself to the `docker` group, so that you can avoid using `sudo`:

```shell
sudo usermod -aG docker ${USER}
```

Before `usermod` takes effect, you will need to log out and log back in again.
After that, please check that this has worked by running `id -nG` and checking that `docker` is listed in your groups.

For other operating systems, or for more details about Docker on Ubuntu, see the official Docker documentation:

* [Windows](https://docs.docker.com/docker-for-windows/install/)
* [Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
* [Mac](https://docs.docker.com/docker-for-mac/install/)

### Serve the site locally

The [`compose.sh`](/compose.sh) script can be used to perform most actions that you will need to run, including serving the site locally:

```shell
$ ./compose.sh serve

Fetching gem metadata from https://rubygems.org/
Fetching gem metadata from https://rubygems.org/.........
Fetching gem metadata from https://rubygems.org/.........
Using public_suffix 4.0.6
Using bundler 2.2.24
...
Using minima 2.5.1
Bundle complete! 7 Gemfile dependencies, 32 gems now installed.
Use `bundle info [gemname]` to see where a bundled gem is installed.
ruby 2.7.1p83 (2020-03-31 revision a0c7c23c9c) [x86_64-linux-musl]
Configuration file: /srv/jekyll/_config.yml
            Source: /srv/jekyll
       Destination: /srv/jekyll/_site
 Incremental build: disabled. Enable with --incremental
      Generating...
       Jekyll Feed: Generating feed for posts
                    done in 1.192 seconds.
 Auto-regeneration: enabled for '/srv/jekyll'
    Server address: http://0.0.0.0:4000//
  Server running... press ctrl-c to stop.

```

### Troubleshooting running the website locally

If the above command does not work, you can initiate serving the website locally on your system via the following command:

```shell
bundle exec jekyll serve
```

And open your browser at [http://localhost:4000/](http://localhost:4000/).

### Building the site on disk

The `serve` command will `build` the website and `serve` it from your computer. However, should you need to build it without running the website locally run the following command:

With `Docker`

```shell
./compose.sh build
```

If the above command does not work try the following:

```shell
bundle exec jekyll build
```

This will build the HTML files and write them to a directory called `_site`.

## Conventions in this code-base

This site uses [bootstrap-sass-3.2.0-4](https://github.com/twbs/bootstrap-sass).
Wherever possible, [Bootstrap](https://getbootstrap.com/) styles should be used, in preference to writing CSS / SCSS by hand.
We want to keep the site consistent, in terms of its look and feel, so we only use a small selection of colours and one font.
These are defined in [`css/_sass`](/css/_sass_).

As far as possible, [Liquid templates](https://shopify.github.io/liquid/) should be used to avoid repetition and improve the [factoring](https://en.wikipedia.org/wiki/Decomposition_(computer_science)) of the code base.
Any large amounts of data should be factored out and placed in YAML files in [`_data`](/_data).

For social media icons, and similar images, we use responsive [Font Awesome icons](https://fontawesome.com/), rather than static images.

The front-matter of each page, should contain the following variables:

```yaml
layout: article
title: About
slug: about.html
navigation_weight: 2
nav:
    - title: About Us
      url: "#intro"
      ...
```

The `article` layout should be used for every page except `index.html`.
The `navigation_weight` defines where in the navigation drawer this page will appear.
The `index.html` page has a `navigation_weight` of 1, and pages further down the list have higher numbers.
The `nav` section defines internal links within the page that should appear in the navigation drawer.

### Important

The `title` of the current page should be identical to the title of the page in the `nav` list, otherwise the navbar dropdown will not appear on the page.
Every page **must** have a `slug` which ends in `.html`.
The URL of each page is made up of three elements: `url/baseurl/slug`.
Both `url` and `baseurl` are defined in the configuration file, usually [`_config.yml`](/_config.yml), and `slug` is defined in the front-matter of each page.

Note that the [`_config.yml_`](/_config.yml) file is intended for development environments *only*.
If you need to change the configuration for deployed environments, please ask the CTO to update the GitLab CI environment variables for you.

## Colours and fonts

Colours, font sizes and font weights should never be hard-coded.
To keep a consistent brand identity, and avoid a confused look and feel, we use a small number of well defined fonts and colours, which are described in our [brand guidelines](https://brand.beautifulcanoe.com/).
The file [`css/_sass/_fonts.scss`](/css/_sass/_fonts.scss) describes the available font sizes and weights in this site, and [`css/_sass/_colours.scss`](/css/_sass/_colours.scss) describes the available colours.

## Updating dependencies

Note that until Issue #93 can be resolved, updating dependencies in the [Gemfile](/Gemfile) will break the CI/CD build, which caches Gems.
For the moment, whenever you change [Gemfile](/Gemfile) and [Gemfile.lock](/Gemfile.lock), you will also need to change the cache key in [.gitlab-ci.yml](/.gitlab-ci.yml).
To generate a new cache key, just run `sha1sum` on the lock file:

```shell
$ sha1sum Gemfile.lock
7c5316a693811b81d900c26038ead0f85a66d91f  Gemfile.lock
$
```

and paste the hash into the `build / cache / key` node in `.gitlab-ci.yml` file.

## Testing the website

Most functional and non-functional features of the site can be checked by serving the site locally, and using [Chrome developer tools](https://developers.google.com/web/tools/chrome-devtools/).
However, it is useful to use external tools to check the site for broken links and good practices in accessibility.
This is automated via the pipeline

### HTML5 compliance

The site should always be HTML5-compliant.
To check this, we use [HTML5 Validator](https://github.com/svenkreiss/html5validator):

```shell
$ ./compose.sh htmllint
$
```

### CSS

We use [Bootstrap](https://getbootstrap.com/) to style the company web page.
Bootstrap makes styling much simpler, and helps to make the site responsive, but it can be easy to break the styling.
[Bootlint](https://github.com/twbs/bootlint) checks for common Bootstrap errors and issues.
It assumes that the site is already HTML5-compliant (i.e. that [HTML5 Validator](https://github.com/svenkreiss/html5validator) runs clean):

```shell
$ ./compose.sh csslint
/usr/local/bin/bootlint -> /usr/local/lib/node_modules/bootlint/src/cli-main.js
+ bootlint@1.1.0
added 51 packages from 22 contributors in 2.486s

0 lint error(s) found across 8 file(s).
$
```

### Broken links

### Checking for broken links

The pipelines also check for broken links, using [LinkChecker](https://linkchecker.github.io/linkchecker/).
You may occasionally need to run the LinkChecker in your development environment, in which case the instructions here should be enough to start you off.
However, we would generally recommend just reading the output of the LinkChecker tool in the pipeline logs on GitLab.

```shell
$ ./compose.sh linkchecker
WARNING linkcheck.check 2021-10-27 16:37:18,246 MainThread Running as root user; dropping privileges by changing user to nobody.
INFO linkcheck.cmdline 2021-10-27 16:37:18,247 MainThread Checking intern URLs only; use --check-extern to check extern URLs.
LinkChecker 9.4.0              Copyright (C) 2000-2014 Bastian Kleineidam
LinkChecker comes with ABSOLUTELY NO WARRANTY!
This is free software, and you are welcome to redistribute it
under certain conditions. Look at the file `LICENSE' within this
distribution.
Get the newest version at https://linkchecker.github.io/linkchecker/
Write comments and bugs to https://github.com/linkchecker/linkchecker/issues

Start checking at 2021-10-27 16:37:18+000

Statistics:
Downloaded: 148.49KB.
Content types: 36 image, 2 text, 0 video, 0 audio, 3 application, 1 mail and 31 other.
URL lengths: min=18, max=124, avg=46.

That's it. 73 links in 73 URLs checked. 0 warnings found. 0 errors found.
Stopped checking at 2021-10-27 16:37:18+000 (0.12 seconds)

$
```

## Testing for accessibility

To check for adherence to common accessibility guidelines, we use [pa11y](https://pa11y.org/) in the GitLab pipeline.
To view the results of `pa11y`, see the `test:a11y` job in each pipeline, and click the **Browse** button under **Job artefacts**.
Alternatively, each merge request will have a **View exposed artefact** link under the link to the latest pipeline.

We also advise developers to run a number of tools within their development environment, whilst they are working.
In particular:

* [Google Lighthouse](https://developers.google.com/web/tools/lighthouse/)
* [Wave accessibility tool](https://wave.webaim.org/)
* [Colorblinding](https://chrome.google.com/webstore/detail/colorblinding/dgbgleaofjainknadoffbjkclicbbgaa?hl=en) (for Chrome) or [Let's Go Color Blind](https://addons.mozilla.org/en-GB/firefox/addon/let-s-get-color-blind/) (for Firefox).

## Ensuring that your Markdown syntax is valid

To make sure that the Markdown documentation in this repository is valid, please use [the mdl Markdown lint](https://github.com/markdownlint/markdownlint).

To install the lint on Debian-like machines, use the [Rubygems](https://rubygems.org/) package manager:

```shell
sudo apt-get install gem
sudo gem install mdl
```

Because we keep each sentence on a separate line, you will want to suppress spurious `MD013 Line length` reports by configuring `mdl`.
The file [.mdl.rb](/.mdl.rb) contains styles that deal with `MD013` and other tweaks we want to make to the lint.
To use the style configuration, pass it as a parameter to `mdl` on the command line:

```shell
mdl -s .mdl.rb DOCUMENT.md
```

If you want to run `mdl` from your IDE or editor, you will either need to configure it, or find a plugin, such as [this one for Sublime Text](https://github.com/SublimeLinter/SublimeLinter-mdl).

## Setting up Git hooks

This repository provides [Git hooks](https://githooks.com/) that will run the `mdl` lint and attempt to build the site, each time the developer commits or pushes their code.
Git will refuse to perform the commit (or push) if either the site does not build, or the `mdl` does not run clean.

To install the hooks, first ensure that you have installed all the tools mentioned in the section on testing.
Next, run the hook install script:

```shell
./bin/create-hook-symlinks
```

Then, when you commit and push, the hooks will check that the site still builds correctly and has no broken links:

```shell
$ git push ...
Fetching gem metadata from https://rubygems.org/
Fetching gem metadata from https://rubygems.org/.........
Fetching gem metadata from https://rubygems.org/.........
Using public_suffix 4.0.6
Using bundler 2.2.24
Fetching chunky_png 1.4.0
Using colorator 1.1.0
Fetching fssm 0.2.10
Installing chunky_png 1.4.0
Installing fssm 0.2.10
Using rb-fsevent 0.11.0
Fetching ffi 1.15.4
Using concurrent-ruby 1.1.9
Using eventmachine 1.2.7
Using http_parser.rb 0.6.0
Using forwardable-extended 2.6.0
Fetching kramdown 1.17.0
Installing kramdown 1.17.0
Installing ffi 1.15.4 with native extensions
Using liquid 4.0.3
Using mercenary 0.3.6
Fetching rouge 3.26.1
Installing rouge 3.26.1
Using safe_yaml 1.0.5
Using addressable 2.8.0
Using i18n 0.9.5
Using em-websocket 0.5.2
Using pathutil 0.16.2
Using rb-inotify 0.10.1
Using sass-listen 4.0.0
Fetching listen 3.7.0
Using sass 3.7.4
Fetching compass 0.12.2
Installing listen 3.7.0
Using jekyll-sass-converter 1.5.2
Using jekyll-watch 2.2.1
Fetching jekyll 3.8.7
Installing jekyll 3.8.7
Using jekyll-feed 0.15.1
Using jekyll-seo-tag 2.7.1
Using jekyll-sitemap 1.4.0
Using minima 2.5.1
Installing compass 0.12.2
Bundle complete! 7 Gemfile dependencies, 32 gems now installed.
Use `bundle info [gemname]` to see where a bundled gem is installed.
ruby 2.7.1p83 (2020-03-31 revision a0c7c23c9c) [x86_64-linux-musl]
Configuration file: /srv/jekyll/_config.yml
            Source: /srv/jekyll
       Destination: /srv/jekyll/_site
 Incremental build: disabled. Enable with --incremental
      Generating...
       Jekyll Feed: Generating feed for posts
                    done in 1.294 seconds.
 Auto-regeneration: disabled. Use --watch to enable.

$
```

## Further reading

* [Beautiful Canoe brand guidelines](https://brand.beautifulcanoe.com/)
* [Bootlint](https://github.com/twbs/bootlint)
* [Bootstrap](https://getbootstrap.com/)
* [Bootstrap SASS](https://github.com/twbs/bootstrap-sass)
* [Chrome developer tools](https://developers.google.com/web/tools/chrome-devtools/)
* [Colorblinding Chrome extension](https://chrome.google.com/webstore/detail/colorblinding/dgbgleaofjainknadoffbjkclicbbgaa?hl=en)
* [Docker](https://www.docker.com/)
* [Font Awesome](https://fontawesome.com/)
* [Google Lighthouse](https://developers.google.com/web/tools/lighthouse/)
* [HTML5 Validator](https://github.com/svenkreiss/html5validator)
* [Jekyll](https://jekyllrb.com/)
* [Let's Go Color Blind Firefox extension](https://addons.mozilla.org/en-GB/firefox/addon/let-s-get-color-blind/)
* [Linkchecker](http://wummel.github.io/linkchecker/)
* [Liquid templating](https://shopify.github.io/liquid/)
* [Markdown lint](https://github.com/markdownlint/markdownlint)
* [openjdk-8-jre](https://openjdk.java.net/install/)
* [pa11y](https://pa11y.org/)
* [pip package manager](https://pypi.org/project/pip/)
* [Responsive web design](https://www.w3schools.com/html/html_responsive.asp)
* [SASS](https://sass-lang.com/)
* [Wave accessibility tool](https://wave.webaim.org/)
