#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

JEKYLL_VERSION=3.8.5
COMMAND=$1

mkdir -p _site

case ${COMMAND} in
    build)
        docker run --rm --volume="${PWD}:/srv/jekyll" jekyll/jekyll:"${JEKYLL_VERSION}" jekyll build
        ;;
    serve)
        docker run --rm --volume="${PWD}:/srv/jekyll" --publish [::1]:4000:4000 jekyll/jekyll:"${JEKYLL_VERSION}" jekyll serve
        ;;
    linkchecker)
        docker run --rm -it  -v "${PWD}"/_site:/mnt linkchecker/linkchecker index.html
        ;;
    htmllint)
        docker run --rm --volume="${PWD}:/app" painless/html5validator html5validator --root _site/
        ;;
    csslint)
        docker run --rm --volume="${PWD}:/app" node:10 npm install bootlint -g && bootlint _site/*.html
        ;;
    *)
        echo "Available commands: build, serve, linkchecker, htmllint, csslint."
        exit 1
        ;;
esac
